﻿// homework18.2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

template <class MyType>
class Stack {

private:
    MyType* bottom;
    MyType* top;
    int _size = 5;

public:
    int getSize()
    {
        return _size;
    }

    Stack(int initSize)
    {
        _size = initSize;
        bottom = new MyType[_size];
        top = bottom;
    }

    ~Stack()
    {
        delete[] bottom;
    }

    int getItemsNumber()
    {
        return top - bottom;
    }

    void resize(int newSize)
    {
        MyType* newArr = new MyType[newSize];

        for (int x = 0; x < getItemsNumber(); x++)
        {
            *(newArr + x) = *(bottom + x);
        }

        delete[] bottom;
        bottom = newArr;

        newArr = nullptr;

        top = bottom + _size;
        _size = newSize;

        std::cout << "array have been resized to size " << newSize << "\n";
    }

    bool isFull()
    {
        return _size == getItemsNumber();

    }

    void push(MyType item)
    {
        if (isFull())
        {
            resize(_size * 2);
        }
        *top = item;
        top++;
    }

    MyType pop()
    {
        top--;
        return *top;
    }

    void printStack()
    {
        for (int x = 0; x < getItemsNumber(); x++)
        {
            std::cout << *(bottom + x) << "\n";
        }
    }
};


int main()
{

    Stack<int> iStack(3);

    iStack.push(1);
    iStack.push(2);
    iStack.push(3);
    iStack.push(4);
    iStack.printStack();
    iStack.push(5);
    iStack.printStack();
    iStack.push(6);
    iStack.push(7);
    iStack.printStack();

    std::cout << "first pop: " << iStack.pop() << "\n";
    std::cout << "second pop: " << iStack.pop() << "\n";
    iStack.printStack();

    Stack<std::string> sStack(2);
    sStack.push(" World!");
    sStack.push(" beautiful");
    sStack.push(" my");
    sStack.push("Hello,");

    sStack.printStack();
    std::cout << sStack.pop();
    std::cout << sStack.pop();
    std::cout << sStack.pop();
    std::cout << sStack.pop();

    sStack.printStack();
}
// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
